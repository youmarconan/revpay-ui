import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { User } from '../models/user';
import { TransactionView } from '../models/transaction-view';
import { Credentials } from '../models/credentials';
import { Observable } from 'rxjs';
import { TransactionRequest } from '../models/transaction-request';
import { RequestView } from '../models/request-view';
import { RequestRequest } from '../models/request-request';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) { }

  user:User ={
    firstName: '',
    lastName: '',
    username: '',
    email: '',
    password: '',
    phoneNumber: '',
    currentBalance: 0
  }

  userNumber!: string;
  
  login(credentials:Credentials):Observable<User>{
    

    return this.http.post<User>("http://localhost:5000/revpay/user/login", credentials)

  
  }

  phoneNumber!: string;

  transactionView: TransactionView[] = [];

  requestView: RequestView[] = [];


  register(user : User): Observable<HttpResponse<any>> {

    return this.http.post("http://localhost:5000/revpay/user/signup", user) as unknown as Observable<HttpResponse<any>>

    
  }

  sendMoney(transactionRequest: TransactionRequest): Observable<HttpResponse<any>> {

    return this.http.put("http://localhost:5000/revpay/user/transactions/sendmoney", transactionRequest) as unknown as Observable<HttpResponse<any>>
    
  }

  viewMyTransactions(phoneNumber: string): Observable<HttpResponse<TransactionView[]>>{


    return this.http.get<TransactionView[]>("http://localhost:5000/revpay/user/transactions/history/" + phoneNumber) as unknown as Observable<HttpResponse<TransactionView[]>>
  
  }

  viewReceivedRequests(phoneNumber: string): Observable<HttpResponse<RequestView[]>>{


    return this.http.get<RequestView[]>("http://localhost:5000/revpay/user/request/received/" + phoneNumber) as unknown as Observable<HttpResponse<RequestView[]>>
  
  }

  sendRequest(requestRequest: RequestRequest): Observable<HttpResponse<any>> {

    return this.http.post("http://localhost:5000/revpay/user/request/send", requestRequest) as unknown as Observable<HttpResponse<any>>
    
  }





}
function subscribe() {
  throw new Error('Function not implemented.');
}

