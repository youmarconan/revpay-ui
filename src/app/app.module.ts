import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MyTransactionsComponent } from './components/my-transactions/my-transactions.component';
import { ReceivedRequestsComponent } from './components/received-requests/received-requests.component';
import { SendMoneyFormComponent } from './components/send-money-form/send-money-form.component';
import { SendRequestFormComponent } from './components/send-request-form/send-request-form.component';
import { Navbar1Component } from './components/navbar1/navbar1.component';
import { Navbar2Component } from './components/navbar2/navbar2.component';
import { UserViewComponent } from './components/user-view/user-view.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    MyTransactionsComponent,
    ReceivedRequestsComponent,
    SendMoneyFormComponent,
    SendRequestFormComponent,
    Navbar1Component,
    Navbar2Component,
    UserViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
