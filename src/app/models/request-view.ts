export class RequestView {
    constructor(
        public requestSender: string,
        public amount: string,
        public status: string,
        public id: number,
    ){}
}
