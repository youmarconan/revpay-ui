export class User {
    constructor(

        public firstName:string,
        public lastName:string,
        public username:string,
        public email:string,
        public password:string,
        public phoneNumber:string,
        public currentBalance: number,
        public id?:number
    
    ){}
}

