export class TransactionRequest {
    constructor(
        public moneySenderPhoneNumber:string,
        public by:string,
        public moneyReceiver:string,
        public amount:number,
    ){}
}
