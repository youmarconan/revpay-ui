export class RequestRequest {
    constructor(
        public requestSenderPhoneNumber:string,
        public by:string,
        public requestReceiver:string,
        public amount:number,
    ){}
}