export class TransactionView {
    constructor(
        public sender:string,
        public receiver:string,
        public time:string,
        public amount:number,
    ){}
}
