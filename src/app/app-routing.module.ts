import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserViewComponent } from './components/user-view/user-view.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { SendMoneyFormComponent } from './components/send-money-form/send-money-form.component';
import { SendRequestFormComponent } from './components/send-request-form/send-request-form.component';
import { MyTransactionsComponent } from './components/my-transactions/my-transactions.component';
import { ReceivedRequestsComponent } from './components/received-requests/received-requests.component';
import { Navbar1Component } from './components/navbar1/navbar1.component';
import { UserService } from './services/user.service';

const routes: Routes = [
  {
    path: "",
    component: Navbar1Component
  },
  {
    path: "user",
    component: UserViewComponent
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "register",
    component: RegisterComponent
  },
  {
    path: "sendmoney",
    component: SendMoneyFormComponent
  },
  {
    path: "sendrequest",
    component: SendRequestFormComponent
  },
  {
    path: "trasactions",
    component: MyTransactionsComponent
  },
  {
    path: "requests",
    component: ReceivedRequestsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [UserService]
})
export class AppRoutingModule { }
