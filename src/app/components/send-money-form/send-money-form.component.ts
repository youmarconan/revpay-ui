import { Component } from '@angular/core';
import { TransactionRequest } from 'src/app/models/transaction-request';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-send-money-form',
  templateUrl: './send-money-form.component.html',
  styleUrls: ['./send-money-form.component.css']
})
export class SendMoneyFormComponent {
  constructor(private userservice:UserService){}

  error: string ="";

  sendMoney(){
    console.log(this.userservice.user)
    this.userservice.sendMoney(this.form).subscribe(

      (data:any) => {
        console.log(data)
      
      }, Error =>{
        this.error = Error.error.message
        console.log(Error)
      })
  }

  form: TransactionRequest = {
    moneySenderPhoneNumber: this.userservice.user.phoneNumber,
    by: '',
    moneyReceiver: '',
    amount: 0
  };
}
