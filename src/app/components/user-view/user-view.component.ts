import { Component } from '@angular/core';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.css']
})
export class UserViewComponent {


  constructor(private userservice:UserService){}


  user:string = this.userservice.user.firstName;
  currentBalance:number = this.userservice.user.currentBalance;

  
}
