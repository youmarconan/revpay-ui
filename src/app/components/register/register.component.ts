import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  constructor(private userservice:UserService,private router:Router){}

  error: string ="";

  register(){
    this.userservice.register(this.user).subscribe(

      (data:any) => {
        console.log(data)
        this.router.navigate(['login']);
      }, Error =>{
        this.error = Error.error.message
        console.log(Error)
        this.user = {
          firstName: '',
          lastName: '',
          username: '',
          email: '',
          password: '',
          phoneNumber: '',
          currentBalance: 0
        }
      })
  }

  user:User = {
    firstName: '',
    lastName: '',
    username: '',
    email: '',
    password: '',
    phoneNumber: '',
    currentBalance: 0
  }
}
