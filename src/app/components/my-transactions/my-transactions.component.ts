import { Component } from '@angular/core';
import { TransactionView } from 'src/app/models/transaction-view';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-my-transactions',
  templateUrl: './my-transactions.component.html',
  styleUrls: ['./my-transactions.component.css']
})
export class MyTransactionsComponent {
  constructor(private userService: UserService){}

  ngOnInit(): void {
    this.viewTransactionHistory();
  }

  viewTransactionHistory(){
    console.log(this.userService.user.phoneNumber)
    console.log(this.userService.phoneNumber)
    this.userService.viewMyTransactions(this.userService.user.phoneNumber).subscribe(

      (data:any) => {
        console.log(data)
        this.transactions = data; 
        console.log(this.transactions)
      }
    )
  }

  transactions: TransactionView[] = [];
}
