import { Component } from '@angular/core';
import { RequestView } from 'src/app/models/request-view';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-received-requests',
  templateUrl: './received-requests.component.html',
  styleUrls: ['./received-requests.component.css']
})
export class ReceivedRequestsComponent {

  constructor(private userService: UserService){}

  ngOnInit(): void {
    this.viewReceivedRequests();
  }

  viewReceivedRequests(){
    this.userService.viewReceivedRequests(this.userService.user.phoneNumber).subscribe(

      (data:any) => {
        console.log(data)
        this.requests = data; 
        console.log(this.requests)
      },
      (Error) =>{
        console.log(Error)
      }
    )
    
  }

  requests: RequestView[] = [];

}
