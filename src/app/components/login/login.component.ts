import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Credentials } from 'src/app/models/credentials';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  constructor(private userService: UserService,private router: Router){}

  message: string = "Log in"

  credentials: Credentials = {
    username: "",
    password: ""
  }

  error: string =""

  user:User = {
    firstName: '',
    lastName: '',
    username: '',
    email: '',
    password: '',
    phoneNumber: '',
    currentBalance: 0
  }
  


  loginFunction() {
     this.userService.login(this.credentials).subscribe(
      (data)=>{
        console.log(data)
        this.user = data;
        this.userService.user=data;
        this.userService.user = this.user
        this.userService.phoneNumber = data.phoneNumber
        this.router.navigate(['user'])
      }
    )
  }
     
}
