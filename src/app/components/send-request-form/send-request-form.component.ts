import { Component } from '@angular/core';
import { RequestRequest } from 'src/app/models/request-request';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-send-request-form',
  templateUrl: './send-request-form.component.html',
  styleUrls: ['./send-request-form.component.css']
})
export class SendRequestFormComponent {

  constructor(private userservice:UserService){}

  error: string ="";

  sendRequest(){
    this.userservice.sendRequest(this.form).subscribe(

      (data:any) => {
        console.log(data)
      
      }, Error =>{
        this.error = Error.error.message
        console.log(Error)
      })
  }

  form: RequestRequest = {
    requestSenderPhoneNumber: this.userservice.user.phoneNumber,
    by: '',
    requestReceiver: '',
    amount: 0
  };
}
