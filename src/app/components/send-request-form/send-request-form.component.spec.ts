import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SendRequestFormComponent } from './send-request-form.component';

describe('SendRequestFormComponent', () => {
  let component: SendRequestFormComponent;
  let fixture: ComponentFixture<SendRequestFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SendRequestFormComponent]
    });
    fixture = TestBed.createComponent(SendRequestFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
